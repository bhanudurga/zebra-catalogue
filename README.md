# Zebra-catalogue

- Ionic 5 mobile application showing data of pictures connecting to localstorage

`Pre Requisites`
- Node js latest version
- Ionic latest version

`Running the application`
- Download or clone the project form the below link
    - https://gitlab.com/bhanudurga/zebra-catalogue.git

- Open the project folder in any editor. Also, open terminal to the project directory.
- Run `npm install` in the terminal to install dependencies.
- To run the project in browser run `ionic serve` in the terminal and application will be served in the browser. 

- To run application in device run the following commands 
	- `ionic cordova run android` (for android emulator)

`Android`
- Run the below command to build the apk file.
     - `ionic cordova build android`

- After the build is successful, Apk is generated in the following folder of the project `platform/android/app/build/outputs/apk/debug` with the name `app-debug.apk `

- Download and install the apk in device to run the application

`iOS`
- Run the below command to build the ipa file
      - `ionic cordova build ios`
