import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  catList: any = [];
  emptyState: boolean = true;
  constructor(public router: Router) {
  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.getData();
  }
  getData() {
    if (JSON.parse(localStorage.getItem('catdata')) == null) {
      this.emptyState = true;
    }
    else {
      this.emptyState = false;
      setTimeout(() => {
        this.catList = JSON.parse(localStorage.getItem('catdata'));
        console.log(this.catList, 'this.catList');
      }, 1000);
    }
  }
  rating(rating, index) {
    this.catList[index].rating = rating;
    localStorage.setItem('catdata', JSON.stringify(this.catList))
  }
  addPicture() {
    this.router.navigateByUrl('/addpicture');
  }
}
