import { Component } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Dashboard', url: '', icon: 'home' },
    { title: 'Add Picture', url: '/addpicture', icon: 'image' },

  ];
  constructor(private diagnostic: Diagnostic) {
    let successCallback = (isAvailable) => { console.log('Is available? ' + isAvailable); };
    let errorExternalCallback = (e) => this.diagnostic.requestExternalStorageAuthorization().then(successCallback).catch(errorcallback);
    let errorCallback = (e) => this.diagnostic.requestCameraAuthorization().then(successCallback).catch(errorcallback);
    let errorCamerarollCallback = (e) => this.diagnostic.requestCameraRollAuthorization().then(successCallback).catch(errorcallback);
    let errorcallback = (e) => console.log(e, 'error');
    this.diagnostic.isCameraAuthorized().then(successCallback).catch(errorCallback);
    this.diagnostic.isExternalStorageAuthorized().then(successCallback).catch(errorExternalCallback);
    this.diagnostic.isCameraRollAuthorized().then(successCallback).catch(errorCamerarollCallback);
   }
}
