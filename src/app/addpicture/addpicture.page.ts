import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { ToastController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
@Component({
  selector: 'app-addpicture',
  templateUrl: './addpicture.page.html',
  styleUrls: ['./addpicture.page.scss'],
})
export class AddpicturePage implements OnInit {
  imageObj: any = {
    'name': '',
    'imageUrl': '',
    'location': '',
    'rating': 0
  }
  constructor(private camera: Camera, private toastCtrl: ToastController,private webview: WebView) { }

  ngOnInit() {
  }
  insertImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 720,
      correctOrientation: true,
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imageObj.imageUrl = this.webview.convertFileSrc(imageData)
    }, (err) => {
      console.log(err);
    });
  }
  save() {
    console.log(this.imageObj, 'this.imageObj');
    let data = JSON.parse(localStorage.getItem('catdata'))
    if (data == null) {
      data = [];
      data.push(this.imageObj);
      localStorage.setItem('catdata', JSON.stringify(data));
      this.showtoast('saved successfully');
      this.reset();
    }
    else {
      data.push(this.imageObj);
      localStorage.setItem('catdata', JSON.stringify(data))
      this.showtoast('saved successfully')
      this.reset();
    }
  }
  rating(rating) {
    this.imageObj.rating = rating;
  }
  reset() {
    this.imageObj = {
      'name': '',
      'imageUrl': '',
      'location': '',
      'rating': 0
    }
  }
  async showtoast(mgs: string, duration: number = 1500, position: any = 'middle') {
    const toast = await this.toastCtrl.create({
      message: mgs,
      duration: 2000,
      position: 'top',
      color: 'dark'
    });
    toast.present();
  }
}
